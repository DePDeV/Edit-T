----#SajjadMazini
DataBase = (loadfile "DataBase.lua")()
-----------------------------
DataBase = DataBase.connect('127.0.0.1', 6379)
-----------------------------
local BOT = Tabchi-ID
-----------------------------
channel_id = DataBase:get('sajjad'..BOT..'channel_id') -- -1001135894458
channel_user = DataBase:get('sajjad'..BOT..'channel_user') -- "@MeGaPlusTeaM"
--------------------------------------
function dl_cb(arg, data)
end
function Check_Info ()
	if DataBase:scard('sajjad'..BOT..'admin') == 0 then
		local admin = nil
		while not admin do
			print("\n\27[36m                      @MeGaPlusTeaM \n >> Admin UserID :\n\27[31m                 ")
			admin=io.read("*n")
		end
		DataBase:del("sajjad"..BOT.."admin")
    	DataBase:sadd("sajjad"..BOT.."admin", admin)
    	print("\n\27[36m     ADMIN ID |\27[32m ".. admin .." \27[36m| شناسه ادمین")
	end
	if not channel_id then
		while not channel_id do
			print("\n\27[36m                      @MeGaPlusTeaM \n >> Channel Id :\n\27[31m                 ")
			channel_id=io.read("*n")
		end
		DataBase:set('sajjad'..BOT..'channel_id', channel_id)
		print("\n\27[36m     channel id |\27[32m ".. channel_id .." \27[36m")
	end
	if (not channel_user or channel_user == "") then
		while (not channel_user or channel_user == "") do
			print("\n\27[36m                      @MeGaPlusTeaM \n >> Channel Username :\n\27[31m                 ")
			channel_user = io.read()
		end
		DataBase:set('sajjad'..BOT..'channel_user', channel_user)
		print("\n\27[36m     channel_user |\27[32m ".. channel_user .." \27[36m")
	end
end
-----------------------------
function get_bot (i, bibak)
	function bot_info (i, bibak)
		DataBase:set("sajjad"..BOT.."id",bibak.id_)
		if bibak.first_name_ then
			DataBase:set("sajjad"..BOT.."fname",bibak.first_name_)
		end
		if bibak.last_name_ then
			DataBase:set("sajjad"..BOT.."lanme",bibak.last_name_)
		end
		DataBase:set("sajjad"..BOT.."num",bibak.phone_number_)
		return bibak.id_
	end
	tdcli_function ({ID = "GetMe",}, bot_info, nil)
end
-----------------------------
function is_bibak(msg)
    local var = false
	local hash = 'sajjad'..BOT..'admin'
	local user = msg.sender_user_id_
    local Bibak = DataBase:sismember(hash, user)
	if Bibak then
		var = true
	end
	return var
end
-----------------------------
function writefile(filename, input)
	local file = io.open(filename, "w")
	file:write(input)
	file:flush()
	file:close()
	return true
end
-----------------------------
function process_join(i, bibak)
	if bibak.code_ == 429 then
		local message = tostring(bibak.message_)
		local Time = message:match('%d+') + 85
		DataBase:setex("sajjad"..BOT.."maxjoin", tonumber(Time), true)
	else
		DataBase:srem("sajjad"..BOT.."goodlinks", i.link)
		DataBase:sadd("sajjad"..BOT.."savedlinks", i.link)
	end
end
function process_link(i, bibak)
	if (bibak.is_group_ or bibak.is_supergroup_channel_) then
		DataBase:srem("sajjad"..BOT.."waitelinks", i.link)
		DataBase:sadd("sajjad"..BOT.."goodlinks", i.link)
	elseif bibak.code_ == 429 then
		local message = tostring(bibak.message_)
		local Time = message:match('%d+') + 85
		DataBase:setex("sajjad"..BOT.."maxlink", tonumber(Time), true)
	else
		DataBase:srem("sajjad"..BOT.."waitelinks", i.link)
	end
end
function find_link(text)
	if text:match("https://telegram.me/joinchat/%S+") or text:match("https://t.me/joinchat/%S+") or text:match("https://telegram.dog/joinchat/%S+") then
		local text = text:gsub("t.me", "telegram.me")
		local text = text:gsub("telegram.dog", "telegram.me")
		for link in text:gmatch("(https://telegram.me/joinchat/%S+)") do
			if not DataBase:sismember("sajjad"..BOT.."alllinks", link) then
				DataBase:sadd("sajjad"..BOT.."waitelinks", link)
				DataBase:sadd("sajjad"..BOT.."alllinks", link)
			end
		end
	end
end
-----------------------------
function add(id)
	local Id = tostring(id)
	if not DataBase:sismember("sajjad"..BOT.."all", id) then
		if Id:match("^(%d+)$") then
			DataBase:sadd("sajjad"..BOT.."users", id)
			DataBase:sadd("sajjad"..BOT.."all", id)
		elseif Id:match("^-100") then
			DataBase:sadd("sajjad"..BOT.."supergroups", id)
			DataBase:sadd("sajjad"..BOT.."all", id)
		else
			DataBase:sadd("sajjad"..BOT.."groups", id)
			DataBase:sadd("sajjad"..BOT.."all", id)
		end
	end
	return true
end
function rem(id)
	local Id = tostring(id)
	if DataBase:sismember("sajjad"..BOT.."all", id) then
		if Id:match("^(%d+)$") then
			DataBase:srem("sajjad"..BOT.."users", id)
			DataBase:srem("sajjad"..BOT.."all", id)
		elseif Id:match("^-100") then
			DataBase:srem("sajjad"..BOT.."supergroups", id)
			DataBase:srem("sajjad"..BOT.."all", id)
		else
			DataBase:srem("sajjad"..BOT.."groups", id)
			DataBase:srem("sajjad"..BOT.."all", id)
		end
	end
	return true
end
-----------------------------
function SendMsg(chat_id, msg_id, text)
	 tdcli_function ({
    ID = "SendChatAction",
    chat_id_ = chat_id,
    action_ = {
      ID = "SendMessageTypingAction",
      progress_ = 100
    }
  }, cb or dl_cb, cmd)
	tdcli_function ({
		ID = "SendMessage",
		chat_id_ = chat_id,
		reply_to_message_id_ = msg_id,
		disable_notification_ = 1,
		from_background_ = 1,
		reply_markup_ = nil,
		input_message_content_ = {
			ID = "InputMessageText",
			text_ = text,
			disable_web_page_preview_ = 1,
			clear_draft_ = 0,
			entities_ = {},
			parse_mode_ = {ID = "TextParseModeHTML"},
		},
	}, dl_cb, nil)
end
-----------------------------
Check_Info()
DataBase:set("sajjad"..BOT.."start", true)
function OffExpire(msg, data)
	SendMsg(msg.chat_id_, msg.id_, "<i>⇜ زمان خاموشی به اتمام رسید و ربات روشن شد ! :)</i>")
end
-----------------------------
function tdcli_update_callback(data)
	if data.ID == "UpdateNewMessage" then
		if DataBase:get("sajjad"..BOT.."OFFTIME") then
			return
		end
		if not DataBase:get("sajjad"..BOT.."maxlink") then
			if DataBase:scard("sajjad"..BOT.."waitelinks") ~= 0 then
				local links = DataBase:smembers("sajjad"..BOT.."waitelinks")
				for x,y in ipairs(links) do
					if x == 6 then DataBase:setex("sajjad"..BOT.."maxlink", 70, true) return end
					tdcli_function({ID = "CheckChatInviteLink",invite_link_ = y},process_link, {link=y})
				end
			end
		end
		if not DataBase:get("sajjad"..BOT.."maxjoin") then
			if DataBase:scard("sajjad"..BOT.."goodlinks") ~= 0 then
				local links = DataBase:smembers("sajjad"..BOT.."goodlinks")
				for x,y in ipairs(links) do
					tdcli_function({ID = "ImportChatInviteLink",invite_link_ = y},process_join, {link=y})
					if x == 2 then DataBase:setex("sajjad"..BOT.."maxjoin", 70, true) return end
				end
			end
		end
		local msg = data.message_
		local bot_id = DataBase:get("sajjad"..BOT.."id") or get_bot()
		if (msg.sender_user_id_ == 777000 or msg.sender_user_id_ == 178220800) then
			local c = (msg.content_.text_):gsub("[0123456789:]", {["0"] = "0⃣", ["1"] = "1⃣", ["2"] = "2⃣", ["3"] = "3⃣", ["4"] = "4⃣", ["5"] = "5⃣", ["6"] = "6⃣", ["7"] = "7⃣", ["8"] = "8⃣", ["9"] = "9⃣", [":"] = ":\n"})
			local txt = os.date("<b>=>New Msg From Telegram</b> : <code> %Y-%m-%d </code>")
			for k,v in ipairs(DataBase:smembers('sajjad'..BOT..'admin')) do
				SendMsg(v, 0, txt.."\n\n"..c)
			end
		end
		if tostring(msg.chat_id_):match("^(%d+)") then
			if not DataBase:sismember("sajjad"..BOT.."all", msg.chat_id_) then
				DataBase:sadd("sajjad"..BOT.."users", msg.chat_id_)
				DataBase:sadd("sajjad"..BOT.."all", msg.chat_id_)
			end
		end
		add(msg.chat_id_)
		if msg.date_ < os.time() - 150 then
			return false
		end
-----------------------------
		if msg.content_.ID == "MessageText" then
    if msg.chat_id_ then
      local id = tostring(msg.chat_id_)
      if id:match('-100(%d+)') then
        chat_type = 'super'
        elseif id:match('^(%d+)') then
        chat_type = 'user'
        else
        chat_type = 'group'
        end
      end
			local text = msg.content_.text_
			local matches
			if DataBase:get("sajjad"..BOT.."link") then
				find_link(text)
			end
	if text and text:match('[qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM]') then
		text = text:lower()
		end
--4829----TexTs-------12242
local Help = [[
<i>Help</i> <code>{MeGaPlus Tabchi}</code>
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥️ جوین روشن/خاموش       
💥 جوین خودکار فعال/غیرفعال
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 اجباری خاموش/روشن     
💥 عضویت اجباری فعال/غیرفعال
💥 عملکر عضویت اجباری به اینصورت است که اگر فعال باشد , هرکس که پی وی تبچی برود , تبچی پیامی حاوی آیدی چنل شما برای فرد ارسال مینماید تا عضو کانال شما شود (کانالی که آیدیشو در ترمینال وارد کردید !) ; تبچی حتما باید در کانال شما ادمین باشد تا این قابلیت کار کند .
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 فوروارد     
💥 فروارد پیام مورد نظر به همه
💥 برروی پیام مورد نظر ریپلای کنید
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 سیک تو همه (یوزرنیم)  
💥 اد کردن کاربر مورد نظر به تمام گروه ها
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 سودو (یوزرنیم)
💥 ترفیع کاربر مورد نظر به مدیر
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 حذف سودو (یوزرنیم)
💥 تنزیل کاربر مورد نظر
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 پینگ                
💥 چک کردن انلاین بودن ربات
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 خروج سوپرگروه/گروه             
💥 خارج شدن از گروه ها
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 ریلود                 
💥 بارگذاری مجدد
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 ریست امار
💥 بازنشانی امار ربات
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
💥 اطلاعات تبچی
💥 دریافت آمار , وضعیت و مشخصات ربات
﹏﹏﹏﹏﹏﹏﹏﹏﹏﹏
نوشته شده توسط [ @SajjadMazini ] در [ @MeGaPlusTeaM ]
]]
local Fwd1 = "⇜ پیام درحال ارسال به همه میباشد ..\n⇜ در هر <code>TIME</code> ثانیه پیام شما به <code>GPSF</code> گروه ارسال میشود .\n⇜ لطفا صبور باشید و تا پایان عملیات دستوری ارسال ننمایید !\n⇜ تا پایان این عملیات <code>ALL</code> ثانیه طول میکشد .\n▪️ ( <code>MIN</code> دقیقه )\n▪️ ( <code>H</code> ساعت )"
local Fwd2 = "🔚 فروارد با موفقیت به اتمام رسید ."
local Done = "<i>⇜ انجام شد .</i>"
local Reload = "⇜ انجام شد .\n⇜ فایل <code>Tabchi"..BOT..".lua</code> با موفقیت بازنگری شد ."
local off = "⇜ انجام شد .\n⇜ ربات به مدت <code>TIME</code> ثانیه خاموش شد !"
local forcejointxt = {'عزیزم اول تو کانالم عضو شو بعد بیا بحرفیم😃❤️\nآیدی کانالم :\n'..channel_user,'عه هنوز تو کانالم نیستی🙁\nاول بیا کانالم بعد بیا چت کنیم😍❤️\nآیدی کانالم :\n'..channel_user,'عشقم اول بیا کانالم بعد بیا پی وی حرف بزنیم☺️\nاومدی بگو 😃❤️\nآیدی کانالم :\n'..channel_user}
local forcejoin = forcejointxt[math.random(#forcejointxt)]
local joinon = "وضعیت عضویت خودکار تغییر کرد به فعال"
local joinoff = "وضعیت عضویت خودکار تغییر کرد به غیرفعال"
local info = [[
<i>Stats & BoT Info</i> <code>{MeGaPlus Tabchi}</code>
➖➖➖➖➖➖➖➖
<b>•⇩ Stats ⇩•</b>

• مگاپلاس تبچی هم اکنون دارای <code>GP</code> گروه , <code>SU</code> سوپرگروه و <code>USR</code> کاربر پی وی می باشد .
➖➖➖➖➖➖➖➖
<b>•⇩ Info ⇩•</b>

• عضویت خودکار مگاپلاس تبچی <code>JO</code> میباشد و تا کنون در <code>JL</code> تا گروه توسط لینک عضو شده است و همچنین <code>WA</code> تا لینک را ذخیره نموده است ! 
• عضویت اجباری بی جی تبچی <code>FORCE</code>  و چنل ذخیره شده [ CHANNL ] می باشد ; تبچی تا در چنل CHANNL ادمین نباشد , عضویت اجباری عمل نمیکند ! 
➖➖➖➖➖➖➖➖
<b>•⇩ About ⇩•</b>

• نام ربات شماره <code> BOT </code>  [ <code>Bibak</code> ] , یوزرآیدی  آن [ <code>ID</code> ] , شماره اکانت آن [ <code>+PH</code> ] می باشد .
➖➖➖➖➖➖➖➖
• @MeGaPlusTeaM
]]
local addtime = {15,16,17,18,19,20,21,23,22,24,25}
local a = addtime[math.random(#addtime)]
local addrandomtime = a
local agpstime = {3,4,5,6,7}
local b = agpstime[math.random(#agpstime)]
local agpsrandom = b
local all = tostring(DataBase:scard("sajjad"..BOT.."groups")) + tostring(DataBase:scard("sajjad"..BOT.."supergroups"))
local eend = ( all / agpsrandom ) * addrandomtime - addrandomtime
local Addall1 = "درحال افزودن ...\nزمانبندی : در هر <code>SLEEP</code> ثانیه کاربر به <code>GP</code> گروه دعوت می شود !\nتا پایان این عملیات <code>END</code> ثانیه زمان صرف میشود و ربات تا پایان این عملیات پاسخگوی دستورات شما نخواهد بود !"
local Addall2 = "افزودن به اتمام رسیدو هم اکنون ربات پاسخگوی شما خواهد بود !"
local sendtime = {25,30,33,35,40,41,42,43,44,45,50,51,52,53,54,55,60}
local kos = sendtime[math.random(#sendtime)]
local randomtime = kos
local gpstime = {3,4,5,6,7}
local kon = gpstime[math.random(#gpstime)]
local gpsrandom = kon
local Fwd1 = "درحال فروارد !\nزمانبندی : در هر <code>TIME</code> ثانیه پیام به <code>RG</code> گروه ارسال میشود .\nتا پایان این عملیات <code>END</code> ثانیه زمان صرف میشود و ربات تا پایان این عملیات پاسخگوی دستورات شما نخواهد بود !"
local Fwd2 = "ارسال به اتمام رسید و هم اکنون ربات پاسخگوی شما خواهد بود !"
local demsudo = "کاربر مورد نظر از مدیریت برکنار شد !" 
local setsudo = "کاربر مورد نظر مدیر شد !"
local rs = "آمار ربات با موفقیت صفر شد !"
local forceon = "وضعیت عضویت اجباری تغییر کرد به فعال"
local forceoff = "وضعیت عضویت اجباری تغییر کرد به غیرفعال"
local gpleave = "• تبچی از <code>GP</code> گروه خارج شد ."
local sgpleave = "• تبچی از <code>SGP</code> سوپرگروه خارج شد ."
local Online = "آنلاین ولی خسته 👁👃👁"
------------------
		if chat_type == 'user' then
local bibak = DataBase:get('sajjad'..BOT..'forcejoin')
if bibak then
if text:match('(.*)') then
function checmember_cb(ex,res)
      if res.ID == "ChatMember" and res.status_ and res.status_.ID and res.status_.ID ~= "ChatMemberStatusMember" and res.status_.ID ~= "ChatMemberStatusEditor" and res.status_.ID ~= "ChatMemberStatusCreator" then
      return SendMsg(msg.chat_id_, msg.id_,forcejoin)
      else
return 
end
end
end
else
if text:match('(.*)') then
return
end
end
tdcli_function ({ID = "GetChatMember",chat_id_ = channel_id, user_id_ = msg.sender_user_id_}, checmember_cb, nil)
    end	
-----------------------------
			if is_bibak(msg) then
				find_link(text)
-----------------------------
								if text:match("^(botoff) (%d+)$") then
					local matches = tonumber(text:match("%d+"))
					DataBase:setex('sajjad'..BOT..'OFFTIME', matches, true)
					tdcli_function ({
					ID = "SetAlarm",
					seconds_ = matches
					}, OffExpire, msg)
					local text = off:gsub("TIME",matches)
					return SendMsg(msg.chat_id_, msg.id_, text)
-----------------------------
				elseif text:match("^(سودو) (%d+)$") then
					local matches = text:match("%d+")
					if DataBase:sismember('sajjad'..BOT..'admin', matches) then
						return SendMsg(msg.chat_id_, msg.id_, "<i>کاربر مورد نظر در حال حاضر مدیر است.</i>")
					elseif DataBase:sismember('sajjad'..BOT..'mod', msg.sender_user_id_) then
						return SendMsg(msg.chat_id_, msg.id_, "شما دسترسی ندارید.")
					else
						DataBase:sadd('sajjad'..BOT..'admin', matches)
						DataBase:sadd('sajjad'..BOT..'mod', matches)
						return SendMsg(msg.chat_id_, msg.id_, setsudo)
					end
-----------------------------
				elseif text:match("^(حذف سودو) (%d+)$") then
					local matches = text:match("%d+")
					if DataBase:sismember('sajjad'..BOT..'mod', msg.sender_user_id_) then
						if tonumber(matches) == msg.sender_user_id_ then
								DataBase:srem('sajjad'..BOT..'admin', msg.sender_user_id_)
								DataBase:srem('sajjad'..BOT..'mod', msg.sender_user_id_)
							return SendMsg(msg.chat_id_, msg.id_, "شما دیگر مدیر نیستید.")
						end
						return SendMsg(msg.chat_id_, msg.id_, "شما دسترسی ندارید.")
					end
					if DataBase:sismember('sajjad'..BOT..'admin', matches) then
						if  DataBase:sismember('sajjad'..BOT..'admin'..msg.sender_user_id_ ,matches) then
							return SendMsg(msg.chat_id_, msg.id_, "شما نمی توانید مدیری که به شما مقام داده را عزل کنید.")
						end
						DataBase:srem('sajjad'..BOT..'admin', matches)
						DataBase:srem('sajjad'..BOT..'mod', matches)
						return SendMsg(msg.chat_id_, msg.id_, demsudo)
					end
					return SendMsg(msg.chat_id_, msg.id_, "کاربر مورد نظر مدیر نمی باشد.")
-----------------------------
	elseif text:match("^(ریلود)$") then
       dofile('./Tabchi-'..BOT..'.lua') 
 return SendMsg(msg.chat_id_, msg.id_, Reload)
-----------------------------
 elseif text:match("^(راهنما)$") then
 return SendMsg(msg.chat_id_, msg.id_, Help)
 -----------------------------
 elseif text:match("^(اجباری روشن)$") then
 DataBase:set("sajjad"..BOT.."اجباری", true)
 return SendMsg(msg.chat_id_, msg.id_, forceon)
 -----------------------------
 elseif text:match("^(اجباری خاموش)$") then
 DataBase:del('sajjad'..BOT..'اجباری')
 return SendMsg(msg.chat_id_, msg.id_, forceoff)
 -----------------------------
 elseif text:match("^(جوین روشن)$") then
DataBase:del("sajjad"..BOT.."maxjoin")
DataBase:del("sajjad"..BOT.."offjoin")
DataBase:set("sajjad"..BOT.."link", true)
 return SendMsg(msg.chat_id_, msg.id_, joinon)
 -----------------------------
 elseif text:match("^(جوین خاموش)$") then
DataBase:set("sajjad"..BOT.."maxjoin", true)
DataBase:set("sajjad"..BOT.."offjoin", true)
--#lakjshxnhasg
DataBase:del("sajjad"..BOT.."link")
 return SendMsg(msg.chat_id_, msg.id_, joinoff)
-----------------------------
				elseif (text:match("^(پینگ)$") and not msg.forward_info_)then
					 return SendMsg(msg.chat_id_, msg.id_, Online)
-----------------------------
					elseif text:match("^(ریست امار)$")then
					local list = {DataBase:smembers("sajjad"..BOT.."supergroups"),DataBase:smembers("sajjad"..BOT.."groups"),DataBase:smembers("sajjad"..BOT.."users")}
				tdcli_function({
						ID = "SearchContacts",
						query_ = nil,
						limit_ = 999999999
					}, function (i, bibak)
						DataBase:set("sajjad"..BOT.."contacts", bibak.total_count_)
					end, nil)
					for i, v in ipairs(list) do
							for a, b in ipairs(v) do 
								tdcli_function ({
									ID = "GetChatMember",
									chat_id_ = b,
									user_id_ = bot_id
								}, function (i,bibak)
									if  bibak.ID == "Error" then rem(i.id) 
									end
								end, {id=b})
							end
					end
					 SendMsg(msg.chat_id_, msg.id_, rs)
-----------------------------					 
					elseif text:match("^(share)$") then
					      get_bot()
					local fname = DataBase:get("sajjad"..BOT.."fname")
					local lnasme = DataBase:get("sajjad"..BOT.."lname") or ""
					local num = DataBase:get("sajjad"..BOT.."num")
					tdcli_function ({
						ID = "SendMessage",
						chat_id_ = msg.chat_id_,
						reply_to_message_id_ = msg.id_,
						disable_notification_ = 1,
						from_background_ = 1,
						reply_markup_ = nil,
						input_message_content_ = {
							ID = "InputMessageContact",
							contact_ = {
								ID = "Contact",
								phone_number_ = num,
								first_name_ = fname,
								last_name_ = lname,
								user_id_ = bot_id
							},
						},
					}, dl_cb, nil)
-----------------------------
					elseif text:match("^(اطلاعات تبچی)$") then
					get_bot()
				local botname = DataBase:get("sajjad"..BOT.."fname")
local botphone = DataBase:get("sajjad"..BOT.."num")
local botuser = DataBase:get("sajjad"..BOT.."id")
local offjoin = DataBase:get("sajjad"..BOT.."offjoin") and "غیرفعال" or "فعال"
local forcejoin = DataBase:get("sajjad"..BOT.."forcejoin") and "فعال" or "غیرفعال"
local gps = tostring(DataBase:scard("sajjad"..BOT.."groups"))
local sgps = tostring(DataBase:scard("sajjad"..BOT.."supergroups"))
local links = tostring(DataBase:scard("sajjad"..BOT.."savedlinks"))
local glinks = tostring(DataBase:scard("sajjad"..BOT.."goodlinks"))
local usrs = tostring(DataBase:scard("sajjad"..BOT.."users"))
local text = info:gsub("GP",gps):gsub("USR",usrs):gsub("SU",sgps):gsub("JL",links):gsub("WA",glinks):gsub("PH",botphone):gsub("Bibak",botname):gsub("ID",botuser):gsub("JO",offjoin):gsub("BOT",BOT):gsub("FORCE",forcejoin):gsub("CHANNL",channel_user)
					return SendMsg(msg.chat_id_, msg.id_, text)
-----------------------------
			elseif (text:match("^(فوروارد)$") and msg.reply_to_message_id_ ~= 0) then 
     			local all = tostring(DataBase:scard("sajjad"..BOT.."all"))
				local bibak = "sajjad"..BOT.."all"
					local endtime = ( all / gpsrandom ) * randomtime - randomtime
						local text = Fwd1:gsub("TIME",randomtime):gsub("END",endtime):gsub("RG",gpsrandom)
				SendMsg(msg.chat_id_, msg.id_, text)
					local list = DataBase:smembers(bibak)
					local id = msg.reply_to_message_id_
						for i, v in pairs(list) do
							tdcli_function({
								ID = "ForwardMessages",
								chat_id_ = v,
								from_chat_id_ = msg.chat_id_,
								message_ids_ = {[0] = id},
								disable_notification_ = 1,
								from_background_ = 1
							}, dl_cb, nil)
							if i % gpsrandom == 0 then
								os.execute("sleep "..randomtime.."")
							end
							end
						return SendMsg(msg.chat_id_, msg.id_, Fwd2)
-----------------------------
	elseif text:match("^(سیک تو همه) (%d+)$") then
					local matches = text:match("%d+")
					local text = Addall1:gsub("SLEEP",addrandomtime):gsub("GP",agpsrandom):gsub("END",eend)
						SendMsg(msg.chat_id_, msg.id_, text)
					local list = {DataBase:smembers("sajjad"..BOT.."groups"),DataBase:smembers("sajjad"..BOT.."supergroups")}
					for a, b in pairs(list) do
						for i, v in pairs(b) do 
							tdcli_function ({
								ID = "AddChatMember",
								chat_id_ = v,
								user_id_ = matches,
								forward_limit_ =  50
							}, dl_cb, nil)
								if i % agpsrandom == 0 then
								os.execute("sleep "..addrandomtime.."")
						end
						end	
					    end
					return SendMsg(msg.chat_id_, msg.id_, Addall2)
-----------------------------
					elseif text:match("^خروج سوپرگروه") then 
					   function lkj(arg, data) 
						bot_id=data.id_ 
						local list = DataBase:smembers('sajjad'..BOT..'supergroups')
						for k,v in pairs(list) do
						DataBase:srem('sajjad'..BOT..'supergroups',v)
						print(v)
						tdcli_function ({
							ID = "ChangeChatMemberStatus",
							chat_id_ = v,
							user_id_ = bot_id,
							status_ = {
							  ID = "ChatMemberStatusLeft"
							},
						  }, dl_cb, nil)
						end
						end
				tdcli_function({ID="GetMe",},lkj, nil)
				           local sgps = tostring(DataBase:scard("sajjad"..BOT.."supergroups"))
				                    local text = sgpleave:gsub("SGP",sgps)
									return SendMsg(msg.chat_id_, msg.id_, text)
--------------------------------------------------------
							elseif text:match("^خروج گروه") then 
					   function lkj(arg, data) 
						bot_id=data.id_ 
						local list = DataBase:smembers('sajjad'..BOT..'groups')
						for k,v in pairs(list) do
						DataBase:srem('sajjad'..BOT..'groups',v)
						print(v)
						tdcli_function ({
							ID = "ChangeChatMemberStatus",
							chat_id_ = v,
							user_id_ = bot_id,
							status_ = {
							  ID = "ChatMemberStatusLeft"
							},
						  }, dl_cb, nil)
						end
						end
				tdcli_function({ID="GetMe",},lkj, nil)
				        local gps = tostring(DataBase:scard("sajjad"..BOT.."groups"))
				              local text = gpleave:gsub("GP",gps)
									return SendMsg(msg.chat_id_, msg.id_, text)
--------------------------------------------------------
				end
					 end 
		elseif msg.content_.ID == "MessageChatDeleteMember" and msg.content_.id_ == bot_id then
			return rem(msg.chat_id_)
		elseif (msg.content_.caption_ and DataBase:get("sajjad"..BOT.."link"))then
			find_link(msg.content_.caption_)
		end
		if DataBase:get("sajjad"..BOT.."markread") then
			tdcli_function ({
				ID = "ViewMessages",
				chat_id_ = msg.chat_id_,
				message_ids_ = {[0] = msg.id_} 
			}, dl_cb, nil)
		end
	elseif data.ID == "UpdateOption" and data.name_ == "my_id" then
		tdcli_function ({
			ID = "GetChats",
			offset_order_ = 9223372036854775807,
			offset_chat_id_ = 0,
			limit_ = 1000
		}, dl_cb, nil)
	end
end
--------------------
-- End Tabchi.lua --
--    By SajjadMazini    --
--------------------
